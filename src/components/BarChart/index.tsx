import { FunctionalComponent, h } from 'preact';
import { useEffect, useRef, useMemo, useCallback, useState } from 'react';
import * as d3 from 'd3';

const MARGIN = { top: 50, right: 50, bottom: 50, left: 50 };

const ICON_SIZE = 40;

type ReactionObject = { value: number; type: string; color: string };

interface Props {
    chartData: Array<ReactionObject>;
    margin?: {
        top: number;
        right: number;
        bottom: number;
        left: number;
    };
    iconSize?: number;
}

const BarChart: FunctionalComponent<Props> = ({ chartData, margin = MARGIN, iconSize = ICON_SIZE }) => {
    const [chartHeight, setChartHeight] = useState(0);
    const [chartWidth, setChartWidth] = useState(0);
    const [isIconsAdded, setIsIconsAdded] = useState(false);

    const xAxisRef = useRef<SVGSVGElement>(null);
    const yAxisRef = useRef<SVGSVGElement>(null);

    const data = useMemo(() => [...chartData].sort((a: ReactionObject, b: ReactionObject) => b.value - a.value), [
        chartData,
    ]);

    const maxValue = useMemo(() => Math.max(...data.map((d: ReactionObject) => d.value)), [data]);

    const yScale = d3
        .scaleLinear()
        .domain([0, maxValue])
        .range([0, chartHeight]);

    const yAxisValues = d3
        .scaleLinear()
        .domain([0, maxValue])
        .range([chartHeight, 0]);

    const yAxisTicks = d3.axisLeft(yAxisValues).ticks(3);

    const xScale = d3
        .scaleBand()
        .range([0, chartWidth])
        .domain(data.map((data: ReactionObject) => data.type))
        .padding(0.1);

    const xAxisTicks = d3.axisBottom(xScale);

    const renderBar = ({ value, color, type }: ReactionObject) => (
        <rect
            key={type}
            fill={color}
            width={xScale.bandwidth()}
            height={yScale(value)}
            y={chartHeight - yScale(value)}
            x={xScale(type)}
        >
            <title>{value}</title>
        </rect>
    );

    const updateChartDimensions = useCallback(() => {
        setChartWidth(window.innerWidth - margin.left - margin.right);
        setChartHeight(window.innerHeight - margin.top - margin.bottom);
    }, [setChartWidth, setChartHeight]);

    useEffect(() => {
        updateChartDimensions();

        // wrap later listeners below with debounce for perfomance optimizations
        window.addEventListener('resize', updateChartDimensions);
        return () => {
            window.removeEventListener('resize', updateChartDimensions);
        };
    }, []);

    useEffect(() => {
        // construct xAxis with images
        if (!isIconsAdded) {
            xAxisRef?.current &&
                d3
                    .select(xAxisRef.current)
                    .call(xAxisTicks)
                    .selectAll('.tick')
                    .append('svg:image')
                    .attr('class', 'reaction-icon')
                    .attr('xlink:href', (d: any) => `./assets/img/${d}.svg`)
                    .attr('width', iconSize)
                    .attr('height', iconSize)
                    .attr('x', iconSize / -2)
                    .attr('y', iconSize / 4)
                    .append('title')
                    .text((d: any) => d);

            d3.selectAll('.tick text').remove();
            setIsIconsAdded(true);
        } else {
            xAxisRef?.current && d3.select(xAxisRef.current).call(xAxisTicks);
        }

        // construct simple yAxis
        yAxisRef?.current && d3.select(yAxisRef.current).call(yAxisTicks);
    }, [chartWidth, chartHeight]);

    return (
        <svg width={chartWidth + margin.right + margin.left} height={chartHeight + margin.top + margin.bottom}>
            <g transform={`translate(${margin.left}, ${margin.bottom - iconSize / 2})`}>{data.map(renderBar)}</g>
            <g>
                <g ref={yAxisRef} transform={`translate(${margin.left}, ${margin.bottom - iconSize / 2})`}></g>
                <g
                    ref={xAxisRef}
                    transform={`translate(${margin.left}, ${chartHeight + margin.top - iconSize / 2})`}
                ></g>
            </g>
        </svg>
    );
};

export default BarChart;
