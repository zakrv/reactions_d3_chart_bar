import { FunctionalComponent, h } from 'preact';
import BarChart from './BarChart';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
if ((module as any).hot) {
    // tslint:disable-next-line:no-var-requires
    require('preact/debug');
}

// replaced initial data format { like: 420, color: 'blue'} 
// to { value: 420, type: 'like', color: 'blue'} for usability reason
const mockData = [
    { value: 420, type: 'like', color: 'blue' },
    { value: 269, type: 'love', color: 'pink' },
    { value: 326, type: 'haha', color: 'yellow' },
    { value: 14, type: 'wow', color: 'orange' },
    { value: 4, type: 'sad', color: 'lightblue' },
    { value: 6, type: 'angry', color: 'red' },
];

const App: FunctionalComponent = () => {
    return (
        <div id="app">
            <BarChart chartData={mockData} />
        </div>
    );
};

export default App;
